package polynomes;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import polynomes.Monome;

public class TestMonome {

  @Test
  public void testToString() {

    assertEquals("+4", new Monome(4, 0).toString());

    assertEquals("+4x", new Monome(4, 1).toString());

    assertThrows(DegreNegatifException.class, () -> {
      new Monome(4, -5).toString();
    });

    try {
      new Monome(4, -5).toString();
      fail("It should've thrown an error!");
    } catch (DegreNegatifException e) {
      assert (true); // t o u t v a b i e n !
    }
  }

  @Test
  public void testMonomeConstructor() {
    Monome m1 = new Monome(4, 0);

    assertEquals(m1.toString(), new Monome(m1).toString());
  }

  @Test
  public void testDerivee() {
    Monome m1 = new Monome(4, 2);

    assertEquals("+8x", m1.derivee().toString());
  }

  @Test
  public void testEval() {
    Monome m1 = new Monome(4, 2);

    assertEquals(100D, m1.eval(5));
  }

}
