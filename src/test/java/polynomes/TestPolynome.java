package polynomes;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import polynomes.*;

public class TestPolynome {
  @Test
  public void testAjoutDeMonome() {
    Polynome p1 = new Polynome();

    Monome m1 = new Monome(1, 5);
    Monome m2 = new Monome(2, 7);

    p1.ajoutMonome(m1);
    p1.ajoutMonome(m2);

    assertEquals("+1x^5+2x^7", p1.toString());

    Polynome p2 = p1;

    assertEquals("+2x^5+4x^7", p1.somme(p2).toString());

    assertEquals("+1x^10+4x^12+4x^14", p1.produit(p2).toString());
  }
}
